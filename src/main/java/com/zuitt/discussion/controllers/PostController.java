package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PostController {
    @Autowired
    private PostRepository postRepository;
//    @GetMapping("/getPost/{id}")
//    @ResponseBody
//    public Optional<Post> findById(@PathVariable(name="id")long id){
//        return postRepository.findById(id);
//
//    }
    @GetMapping("/getPost/{title}")

    public ResponseEntity<List<Post>> findByName(@RequestParam(name="title") String title){
        return new ResponseEntity<List<Post>>(postRepository.findByContent(title), HttpStatus.OK);

    }
}